<?php
    class animal
    {
        public $name;
        public $legs = 4;
        public $cold_blooded = "no";
        
        public function __construct($string)
        {
            $this ->name = $string;
        }

        public function jump(){
            echo "Jump : hop hop <br>";
        }

        public function yell(){
            echo "Yel-Yel : Auooo <br>";    
        }

    }
    
?>    